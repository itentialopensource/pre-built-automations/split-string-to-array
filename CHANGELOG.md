
## 0.0.14 [02-03-2024]

* make changes for deprecation

See merge request itentialopensource/pre-built-automations/split-string-to-array!8

---

## 0.0.13 [05-24-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/split-string-to-array!7

---

## 0.0.12 [06-07-2022]

* Patch/dsup 1361

See merge request itentialopensource/pre-built-automations/split-string-to-array!6

---

## 0.0.11 [12-03-2021]

* Changed README to reflect  current version

See merge request itentialopensource/pre-built-automations/split-string-to-array!5

---

## 0.0.10 [07-01-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/split-string-to-array!4

---

## 0.0.9 [05-13-2021]

* Update README.md

See merge request itentialopensource/pre-built-automations/split-string-to-array!3

---

## 0.0.8 [12-23-2020]

* README 2020.2 updates

See merge request itentialopensource/pre-built-automations/split-string-to-array!2

---

## 0.0.7 [07-08-2020]

* patch/README.md [LB-352]

See merge request itentialopensource/pre-built-automations/staging/splitJST!1

---

## 0.0.6 [07-08-2020]

* patch/README.md [LB-352]

See merge request itentialopensource/pre-built-automations/staging/splitJST!1

---

## 0.0.5 [07-08-2020]

* patch/README.md [LB-352]

See merge request itentialopensource/pre-built-automations/staging/splitJST!1

---

## 0.0.4 [07-06-2020]

* patch/README.md [LB-352]

See merge request itentialopensource/pre-built-automations/staging/splitJST!1

---

## 0.0.3 [07-06-2020]

* Bug fixes and performance improvements

See commit f6deff5

---

## 0.0.2 [07-06-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n
