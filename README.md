<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 01-15-2024 and will be end of life on 01-15-2025. The capabilities of this Pre-Built have been replaced by the [IAP - Data Manipulation](https://gitlab.com/itentialopensource/pre-built-automations/iap-data-manipulation)

# Split String to Array

## Table of Contents

* [Overview](#overview)
* [Installation Prerequisites](#installation-prerequisites)
* [How to Install](#how-to-install)
* [How to Run](#how-to-run)

## Overview

This JST allows IAP users to use native JS String's split inside of their IAP instance with a Regex or a String. It closesly emulates the functionality of the [native JS String split method](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/split). The only noticeable difference is that one must specify whether a Regex or String is used as a separator (where string is the default). See [How to Run](#how-to-run) for more details.

## Installation Prerequisites

Users must satisfy the following prerequisites:

<!-- Include any other required apps or adapters in this list -->
<!-- Ex.: EC2 Adapter -->
* Itential Automation Platform
  * `^2022.1.x`

## How to Install

To install the pre-build:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Prerequisites](#installation-prerequisites) section. 
* The pre-build can be installed from within `App-Admin_Essential`. Simply search for the name of your desired pre-build and click the install button.

## How to Run

Use the following to run the pre-build:

1. Once the JST is installed as outlined in the [How to Install](#how-to-install) section above, navigate to the workflow where you would like to use `String split()` and add a `JSON Transformation` task. 

2. Inside the `Transformation` task, search for and select `split` (the name of the internal JST).

3. The inputs to the JST are the same as `String split`; however, instead of one separator field, there are two: `separatorString` and `separatorRegex`. They correspond to whether a String or Regex should be used as a separator. If both are included, `separatorString` is used by default and the Regex is ignored.

4. Save your input and the task is ready to run inside of IAP.
